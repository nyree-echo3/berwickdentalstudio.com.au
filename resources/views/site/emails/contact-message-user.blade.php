<!DOCTYPE html>
<html>
<body>
<img src="{{ url('') }}/images/site/email-logo.png"><br><br>
Thank you for submitting your enquiry.<br />
A Berwick Dental Studio representative will contact you shortly.<br /><br />
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
