<?php 
   // Set Meta Tags
   $meta_title_inner = (isset($page) ? $page->meta_title : ""); 
   $meta_keywords_inner = (isset($page) ? $page->meta_keywords : ""); 
   $meta_description_inner = (isset($page) ? $page->meta_description : "");  
?>
@extends('site/layouts/app')


@if(isset($page))
@section('styles')
<link rel="canonical" href="{{ url($page->url) }}" />

<meta property="og:title" content="{{ $page->title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ url($page->url) }}" />
@if($page->first_image!='')
<meta property="og:image" content="{{ url($page->first_image) }}" />
@endif
@endsection
@endif

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-pages')        
        
        <div class="col-lg-9 blog-main">

          <div class="blog-post page">
            @if (isset($page))
				<h1 class="blog-post-title {{($page->slug == 'invisalign-for-you' ? "invisalign-h1" : "")}}">{{$page->title}}</h1>
				{!! $page->body !!}      
                
                @include('site/partials/page-footer')
                
                @if ($page->contact == "active")
                    @include('site/partials/call-to-action')
                @endif  
            @endif
                        
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
