<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Blog" ? $category_name : $category_name . " - News"); 
   $meta_keywords_inner = "News"; 
   $meta_description_inner = ($category_name == "Blog" ? $category_name : $category_name . " - News");  
?>
@extends('site/layouts/app')

@section('styles')
    <link rel="canonical" href="{{ url($canocical_url) }}" />
@endsection

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-news')
        
        <div class="col-lg-9 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">{{ $category_name }}</h1>
            @if(isset($items))    
                  <div class="container">	  
                     <div class="row">	   
                                                                              
                  @foreach($items as $news_item)  
                  <div class="col-lg-4 news-list-item">                             
					  <!--<div class='news-list-item'>							    						    
					    <div class="news-list-item-txt">	  -->
							<h2 class="blog-post-title">{{$news_item->title}}</h2>
							
							@if($news_item->thumbnail != "")
				                <div class="news-list-item-img">
					               <img src="{{ url('') }}/{{ $news_item->thumbnail }}" alt="{{$news_item->title}}" width="140" height="140" />	
					            </div>	
					         @endif
							
							{!! $news_item["short_description"] !!}
							<a class='news-more' href='{{ url('').'/'.$news_item->url }}'>more ></a>
						<!--</div>-->
						
						
				             
					   
					  </div>                                                          
                   @endforeach
                   
					  </div>
			  </div>
                  
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no blog items to display.</p>    
               @endif
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
