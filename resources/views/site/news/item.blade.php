<?php 
   // Set Meta Tags
   $meta_title_inner = $news_item->meta_title; 
   $meta_keywords_inner = $news_item->meta_keywords; 
   $meta_description_inner = $news_item->meta_description;  
?>
@extends('site/layouts/app')

@section('styles')
<link rel="canonical" href="{{ url($news_item->url) }}" />
<meta property="og:title" content="{{ $news_item->title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ url($news_item->url) }}" />
@if($news_item->thumbnail != "")
    <meta property="og:image" content="{{ url($news_item->thumbnail) }}" />
@endif
@endsection

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-news')        
        
        <div class="col-lg-9 blog-main">

          <div class="blog-post">                          
            <h1 class="blog-post-title">{{ $news_item->title }}</h1>
            
            <div class='news-item'>
               <div class='news-item-txt'>
			      {!! $news_item->body !!}
			   </div>
          	   
          	   <div class='news-item-img'>
          	      @if($news_item->thumbnail != "")
			      <img src='{{ url('') }}/{{ $news_item->thumbnail }}' alt='{{ $news_item->title }}'>
			      @endif
			   </div>
          	 </div>
         	               	 
           	 <a class='news-more' href='{{ url('') }}/blog/{{ $news_item->category->slug }}'><i class='fa fa-chevron-left'></i> back</a>	
			 
          	   
           	 @include('site/partials/helper-sharing')	             					  								  			            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
