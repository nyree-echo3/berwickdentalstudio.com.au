@php
    if(isset($mode) && $mode == 'preview')
      $meta_title_inner = "Preview Page";
@endphp

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{ (isset($meta_keywords_inner) ? $meta_keywords_inner : $meta_keywords) }}">
    <meta name="description" content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">
    <meta name="date" content="{{ $live_date }}" scheme="DD-MM-YYYY">
    <meta name="robots" content="all">
    <title>{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100,200,300,400,700" rel="stylesheet">
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link href="{{ asset('/css/site/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/base/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/general.css') }}">

    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">

    @yield('styles')
    @yield('styles-cta')
    
    <!-- Google Analytics -->
    {!! $google_analytics !!}

    <!-- Structured Data -->
    {!! $structured_data !!}

</head>
<body>
<header>
    @include('site/partials/preview')
      
    @include('site/partials/navigationV2')
    
</header>

<main role="main">
    @include('site/partials/home-contact-us')
    @yield('content')
    @include('site/partials/instagram')
    @include('site/partials/footer')
</main>

@yield('inline-scripts-instagram')
<script defer src="https://connect.podium.com/widget.js#API_TOKEN=4096282b-f85c-40c1-897d-f92a724028bd" id="podium-widget" data-api-token="4096282b-f85c-40c1-897d-f92a724028bd"></script>
<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>
<script src="{{ asset('js/site/scrollReveal.js') }}"></script>
<script src="{{ asset('js/site/contact-toggle.js') }}"></script>

@yield('scripts')
@yield('inline-scripts')
@yield('contact-scripts')
@yield('boxes-scripts')
@yield('cta-scripts')


<script>
	!function(t){t.fn.bcSwipe=function(e){var n={threshold:50};return e&&t.extend(n,e),this.each(function(){function e(t){1==t.touches.length&&(u=t.touches[0].pageX,c=!0,this.addEventListener("touchmove",o,!1))}function o(e){if(c){var o=e.touches[0].pageX,i=u-o;Math.abs(i)>=n.threshold&&(h(),t(this).carousel(i>0?"next":"prev"))}}function h(){this.removeEventListener("touchmove",o),u=null,c=!1}var u,c=!1;"ontouchstart"in document.documentElement&&this.addEventListener("touchstart",e,!1)}),this}}(jQuery);

	// Swipe functions for Bootstrap Carousel
	$('#myCarousel').bcSwipe({ threshold: 50 });	
	
    function btnSlider_onclick(id)  {
		//$('.divSliderShow').removeClass("divSliderShow").addClass("divSliderHide");
		//$('#divSlider' + id).addClass("divSliderShow");	
		
		$('html,body').animate({
        scrollTop: $('#divSlider' + id).offset().top - 150},
        'slow');
		
		return true;
	}

    $( document ).ready(function() {
        $('.juiAccordion').accordion();
    });
</script>
</body>
</html>