@extends('site/layouts/app')

@section('styles')
    <link rel="canonical" href="{{ url('/') }}" />
@endsection

@section('content')

    @include('site/partials/carousel')

    <div class="introWrapper">
        <div class="container marketing">

            <!-- Intro Text -->
            <div class="row featurette">
                <div class="col home-intro">
                    <?php echo $home_intro_text; ?>
                </div>
            </div>

        </div>

        @include('site/partials/home-boxes')
    </div>

    @include('site/partials/home-sliders-text')

@endsection