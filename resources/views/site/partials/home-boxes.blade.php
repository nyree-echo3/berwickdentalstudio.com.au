<div id="boxWrapper">
	<div class="main-box" data-sr="enter top">
		<a href='{{ url('') }}/invisalign-orthodontics'>
		<img src="{{ url('') }}/images/site/icon1.png">
		<h1 class="main-box-alt">Invisalign & Braces</h1>
		</a>
	</div>	

	<div class="main-box" data-sr="enter top">
		<a href='{{ url('') }}/dental-implants'>
		<img src="{{ url('') }}/images/site/icon2.png">
		<h1 class="main-box-alt">Implants</h1>
		</a>
	</div>

	<div class="main-box" data-sr="enter top">
		<a href='{{ url('') }}/veneers'>
		<img src="{{ url('') }}/images/site/icon3.png">
		<h1 class="main-box-alt">Veneers</h1>
		</a>
	</div>	

	<div class="main-box" data-sr="enter bottom">
		<a href='{{ url('') }}/kids-dentistry'>
		<img src="{{ url('') }}/images/site/icon4.png">
		<h1 class="main-box-alt">Children's</h1>
		</a>
	</div>

	<div class="main-box" data-sr="enter bottom">
		<a href='{{ url('') }}/teeth-whitening'>
		<img src="{{ url('') }}/images/site/icon5.png">
		<h1 class="main-box-alt">Whitening</h1>
		</a>
	</div>

	<div class="main-box" data-sr="enter bottom">
		<a href='{{ url('') }}/payment-plans'>
		<img src="{{ url('') }}/images/site/icon6.png">
		<h1 class="main-box-alt">Payment Plan</h1>
		</a>
	</div>						
</div>

@if($home_team_image!=null)
<img src="{{ url($home_team_image) }}" class="img-fluid" alt="Berwick Dental Studio Team">
@endif

<div class="embed-responsive embed-responsive-16by9 index-video-container">
	<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ku1g2qMcKS0"></iframe>
</div>

@section('boxes-scripts')
<script type="text/javascript">
      (function($) {
		  if( window.innerWidth > 767 ) {

        'use strict';

        window.sr= new scrollReveal({
          reset: true,
          move: '50px',
          mobile: true
        });
		  }
		   else if( window.innerWidth < 767 ) { //do not display on small mobiles like iphone 4 and 5

        'use strict';

        window.sr= new scrollReveal({
          reset: true,
          move: '50px',
          mobile: false
        });
		  }
      })();
</script>
@endsection