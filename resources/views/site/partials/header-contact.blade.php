<div id="blueTab" class="blueTab blueTab-smaller">
	<div class="blueTabRow" onclick="return gtag_report_conversion_bn('book-now')" >
		<script data-he-id="66956" data-he-button="true" data-he-img="HE_BOOKNOW_1.png"
				src="https://healthengine.com.au/webplugin/appointments.js"></script>
	</div>
	<div class="blueTabRow"><a onclick="return gtag_report_conversion('tel:0390024060');"
							   href='tel:{{ str_replace(")", "", str_replace("(", "", str_replace(' ', '', $phone_number))) }}'>{{ $phone_number }}</a></div>
</div>

 

 <div id="blueTab-mobile" class="blueTab-mobile">
  <div class="blueTabRow-mobile">
		<script data-he-id="66956" data-he-button="true" data-he-img="HE_BOOKNOW_1.png"
				src="https://healthengine.com.au/webplugin/appointments.js"></script>
	</div>
	<div class="blueTabRow-mobile"><a href='tel:0390024060'>{{ $phone_number }}</a></div>
</div>