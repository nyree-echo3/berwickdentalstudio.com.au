<div class="featuredSlidersWrapper">  
    @if (isset($images)) 
		@foreach($images as $image)               
			<div id="divSlider{{ $image->id }}" class="featuredSliders">		 
			  @if ( $image->title != "") <h2>{{ $image->title }}</h2> @endif
			  @if ( $image->description != "") <h3>{{ $image->description }}</h3> @endif				  
			  @if ( $image->detailed_description != "") <p>{!! $image->detailed_description !!}</p> @endif	

			  <div class='home-btn-more'><a id='contact-toogle-link' onclick='btnContactToogle_onclick()'>click for more ></a></div>			  
			 </div>		
		@endforeach   
	@endif
</div>
  
