<div class="col-lg-3 blog-sidebar">
	<div class="sidebar-module">
		<h4>{{ $category[0]->name }}</h4>
		<ol class="list-unstyled">
			{!! $side_nav !!}
		</ol>
	</div>
</div>