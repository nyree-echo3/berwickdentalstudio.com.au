<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner inside-page">           
			<div class="carousel-item active">
		  	
		      @php
	            $imgUrl = "images/site/inside-top.jpg";
	            $imgAlt = "";
	            
	            // Page Module Category
		        if (isset($category) && sizeof($category) > 0)  {
		           $imgUrl = $category[0]->header;
		           $imgAlt = $category[0]->name;
		        }	
		       
		        // Module
		        if (isset($module))  {	         
		           $imgUrl = $module->header_image;
		           $imgAlt = $module->display_name;		        
		        }
		        	 	        	        
		      @endphp
		      
			  <img class="slide" src="{{ url('') }}/{{ $imgUrl }}" alt="{{ $imgAlt }}">
			  <div class="container">
				<div class="carousel-caption text-left">{{ $imgAlt }}					  
				</div>
			  </div>
			</div>
       
  </div>
</div>