<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top btco-hover-menu {{ (isset($mode) && $mode == 'preview' ? "fixed-top-preview" : "") }}">	
    <div id="blueTab" class="blueTab blueTab-smaller {{ (isset($mode) && $mode == 'preview' ? "blueTab-preview" : "") }}">
		<div class="blueTabRow blueTabRow-book" onclick="return gtag_report_conversion_bn('book-now')" >
			<script data-he-id="66956" data-he-button="true" data-he-img="HE_BOOKNOW_1.png" src="https://healthengine.com.au/webplugin/appointments.js"></script>
		</div>
		
		<div class="blueTabRow blueTabRow-phone">
		   <a onclick="return gtag_report_conversion('tel:0390024060');" href='tel:{{ str_replace(")", "", str_replace("(", "", str_replace(' ', '', $phone_number))) }}'>{{ $phone_number }}</a>
        </div>

		<div class="blueTabRow blueTabRow-button">						   						   
			<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
		</div>
    </div>
    
    <div class="navbar-logo">
       <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
    </div>
  
	
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
            <li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
                <a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span
                            class="sr-only">(current)</span></a>
            </li>
            {!! $navigation !!}
        </ul>
	</div>
</nav>