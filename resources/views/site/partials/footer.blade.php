<footer class='footer'>	 
		  <div class="row">		     
				  <div class="col-lg-4 footer-logo"><img src="{{ url('') }}/images/site/logo.png" alt="{{ $company_name }}"></div>				    
                            
			      <div class="col-lg-4">
					  <h2>Our Location</h2>
					  <div class="footer-txt">@if ( $address != ""){{ $address }}@endif</div>					  
					  <div class="footer-txt">@if ( $phone_number != "")Phone: <a href="tel:{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", $phone_number))) }}"> {{ $phone_number }}</a>@endif </div>					 
					  <div class="footer-txt">@if ( $email != "")Email: <a href="mailto:{{ $email }}"> {{ $email }}</a>@endif </div>	
					  
					  <h2 class="h2-second">Clinic Hours</h2>
					  <div class="footer-txt">@if ( $clinic_hours != ""){!! $clinic_hours !!}@endif</div>	
			      </div>        
              
				  <div class="col-lg-4"><img src="{{ url('') }}/images/site/map.png" alt="{{ $company_name }}"></div>
		   </div>

	<div id="footer-txt"> 
		@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a> | @endif     
		<a href="{{ url('') }}/contact">Contact</a>        
	</div>
	  
	<div id="footer-social">
		@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
		@if ( $social_twitter != "") <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif 
		@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif
		@if ( $social_googleplus != "") <a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a> @endif
		@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
		@if ( $social_pinterest != "") <a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a> @endif
		@if ( $social_youtube != "") <a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube'></i></a> @endif		
	</div> 

	  
</footer>