<div class="col-lg-3 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Archived Blogs</h4>
	<select name="age" onChange="location.href='{{ url('') }}/news/archive/'+this.value;">
        <option value="0" {{ ($age == 0 ? "selected" : "") }}>Under 6 months</option>
        <option value="6" {{ ($age == 6 ? "selected" : "") }}>Over 6 months</option>
        <option value="12" {{ ($age == 12 ? "selected" : "") }} >Over 1 year</option>
        <option value="24" {{ ($age == 24 ? "selected" : "") }} >Over 2 years</option>
        <option value="36" {{ ($age == 36 ? "selected" : "") }} >Over 3 years</option>                    
    </select>
	
	<ol class="list-unstyled">
       <li class='list-group-item'><i class='fas fa-chevron-right'></i><a class='navsidebar' href='{{ url('') }}/blog'>View Blogs</a></li>
	</ol>
  </div>          
</div>