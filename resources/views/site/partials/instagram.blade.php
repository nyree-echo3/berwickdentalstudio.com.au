<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>

<!-- Instagram Feed https://github.com/BanNsS1/jquery.instagramFeed -->
<script src="{{ url('') }}/js/site/jquery.instagramFeed.js?v=1.0"></script>

<!-- Slideshow Cycle http://jquery.malsup.com/cycle2/demo/-->
<script type="text/javascript" src="{{ url('') }}/js/site/jquery.cycle2.js"></script>
<script src="{{ url('') }}/js/site/jquery.cycle2.carousel.min.js"></script>

<div class="instagramPanel">
   <div id="instagram-panel-embedded" class="carousel-instagram-embedded"
       data-cycle-pause-on-hover="true"
       data-cycle-fx="carousel"
       data-cycle-timeout="2000"
       data-cycle-carousel-visible="5"
       data-cycle-carousel-fluid="true"
       data-cycle-slides="> span"
   >
</div>
</div>

@section('inline-scripts-instagram')  
<script>
	//$( document ).ready(function() {
		$('#instagram-panel-embedded').cycle(); 
		
		$.fn.cycle.defaults.autoSelector = '.carousel-instagram-embedded';

		(function($){		
			$(window).on('load', function(){
				$.instagramFeed({
					'username': 'berwickdentalstudio',
					'container': "#instagram-panel-embedded",
					'display_profile': false,
					'display_biography': false,
					'display_gallery': true,
					'get_raw_json': false,
					'callback': null,
					'styling': false,
					'items': 8,
					'items_per_row': 5,
					'margin': 1 
				});

			});

		})(jQuery);		
	//});	
</script>
@endsection