<div id="contact-slider">

    <div class="contact-slider-content">

        <div class='side-contact'>
            <h2>Contact Us</h2>
            <h3>Fill in the form fields</h3>
            <form id="sideContactForm" name="sideContactForm" action="{{ url('') }}/contact/save-message" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class='input-full'>
                    <div class='input-left'>
                        <input name="name" type='textbox' placeholder="Name" required="required">
                    </div>
                    <div class='input-right'>
                        <input name="email" placeholder="Your email" type="email" required="required">
                    </div>
                </div>

                <input name="phone" type="tel" placeholder="Phone Number" required="required">
                <input name="message" type='textbox' placeholder="Message" required="required">

                <div class="side-contact-wrapper">
                    <div class="col-12 col-sm-10 g-recaptcha-container">
                        <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                        @if ($errors->has('g-recaptcha-response'))
                            <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                        @endif
                    </div>

                    <div class='contactBtn'>
                        <button class="btn-submit" type="submit">Submit</button>
                    </div>
                </div>
            </form>


            <div class="divContact"><p>OR</p>Call us at <a href='tel:0390024060'>(03) 9002 4060</a></div>
        </div>


    </div>
    <div id="contact-slider-button" class="contact-slider-button"></div>
</div>

@section('contact-scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#contact-slider-button').click(function () {
                if ($('#contact-slider').css('right') == "0px") {
                    $('#contact-slider').animate({'right': '-648px'}, 500);

                } else {
                    $('#contact-slider').animate({'right': '0px'}, 500);
                }


            });

        });
    </script>
@endsection