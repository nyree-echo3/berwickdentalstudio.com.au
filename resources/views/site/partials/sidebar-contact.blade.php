<div class="col-lg-3 blog-sidebar">         
  <div class="sidebar-module">  
  <h4>Contact Us</h4>
  	
	<div class="contact-details">
       <strong>{!! $company_name !!}</strong>
	   {!! $contact_details !!}	   
	</div>	
  </div>          
</div>