<div class="col-lg-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>Blog</h4>
        <ol class="list-unstyled">
            @if($side_nav_mode=='manual')
                {!! $side_nav !!}
            @endif
            @if($side_nav_mode=='auto')
                @foreach ($side_nav as $item)
                    <li class='list-group-item'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
                @endforeach                                
            @endif
            
            <li class='list-group-item'><i class='fas fa-chevron-right'></i><a class='navsidebar' href='{{ url('') }}/blog/archive'>View Archives</a></li>
        </ul>
        
            

    </div>
</div>