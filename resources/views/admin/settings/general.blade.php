@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Settings</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">General</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">General Values</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="form-group {{ ($errors->has('company_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Company Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="{{ $company_name->value }}">
                                        @if ($errors->has('company_name'))
                                            <small class="help-block">{{ $errors->first('company_name') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Contact Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" placeholder="Contact Email" value="{{ $email->value }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('phone_number')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" value="{{ $phone_number->value }}">
                                        @if ($errors->has('phone_number'))
                                            <small class="help-block">{{ $errors->first('phone_number') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('fax_number')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Fax Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="fax_number" placeholder="Fax Number" value="{{ $fax_number->value }}">
                                        @if ($errors->has('fax_number'))
                                            <small class="help-block">{{ $errors->first('fax_number') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('address')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="address" rows='5' placeholder="Address">{{ $address->value }}</textarea>
                                        @if ($errors->has('address'))
                                            <small class="help-block">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('clinic_hours')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Clinic Hours</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="clinic_hours" rows='5' placeholder="Clinic Hours">{{ $clinic_hours->value }}</textarea>
                                        @if ($errors->has('clinic_hours'))
                                            <small class="help-block">{{ $errors->first('clinic_hours') }}</small>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            
                            <div class="box-header with-border">
								<h3 class="box-title">Website</h3>
							</div>
                                                                                                  
                            <div class="box-body">

                                <div class="form-group{{ ($errors->has('live_date')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Go Live Date</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="live_date" type="text" class="form-control pull-right datepicker" value="{{ $live_date->value }}">
                                        </div>
                                        @if ($errors->has('live_date'))
                                            <small class="help-block">{{ $errors->first('live_date') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('google_analytics')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Google Analytics</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="google_analytics" rows='10' placeholder="Google Analytics Script">{{ $google_analytics->value }}</textarea>
                                        @if ($errors->has('google_analytics'))
                                            <small class="help-block">{{ $errors->first('google_analytics') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('structured_data')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Structured Data</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="structured_data" rows='10' placeholder="Structured Data">{{ $structured_data->value }}</textarea>
                                        @if ($errors->has('structured_data'))
                                            <small class="help-block">{{ $errors->first('structured_data') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
							</div>
                                                                                                  
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {  
			CKEDITOR.replace('clinic_hours');
			
			$('.datepicker').datepicker({
              autoclose: true,
			  format: 'dd/mm/yyyy'
            });
        });
    </script>
@endsection