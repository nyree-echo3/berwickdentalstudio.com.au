<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Property;
use App\PropertyCategory;
use App\PropertyFloorplan;
use App\PropertyImage;
use App\TeamMember;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\SpecialUrl;
use App\Module;

class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $module_details = Module::where('slug', '=', 'properties')->first();
        view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $properties = Property::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $properties = Property::with('category')->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('properties-filter');
        $categories = PropertyCategory::orderBy('position', 'desc')->get();
        return view('admin/properties/properties', array(
            'properties' => $properties,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = PropertyCategory::orderBy('position', 'desc')->get();
        $agents = TeamMember::orderBy('position', 'desc')->get();
        $images = array();
        $floorplans = array();

        if(old('imageCount')){

            for($i=0; $i<old('imageCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('imgId'.$i);
                $properties->position = old('imgPosition'.$i);
                $properties->delete = old('imgDelete'.$i);
                $properties->status = old('imgStatus'.$i);
                $properties->location = old('imgLocation'.$i);
                $properties->type = old('imgType'.$i);

                $images[$i] = $properties;
            }

            $images = collect($images)->sortBy('position');
        }

        if(old('floorplanCount')){

            for($i=0; $i<old('floorplanCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('flrId'.$i);
                $properties->position = old('flrPosition'.$i);
                $properties->delete = old('flrDelete'.$i);
                $properties->status = old('flrStatus'.$i);
                $properties->location = old('flrLocation'.$i);
                $properties->type = old('flrType'.$i);

                $floorplans[$i] = $properties;
            }

            $floorplans = collect($floorplans)->sortBy('position');
        }

        return view('admin/properties/add', array(
            'categories' => $categories,
            'agents' => $agents,
            'images' => $images,
            'floorplans' => $floorplans
        ));
    }

    public function edit($property_id)
    {
        $property = Property::where('id', '=', $property_id)->with("images")->first();

        $property->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $property->id)->where('module', '=', 'properties')->where('type', '=', 'item')->first();
        if ($special_url) {
            $property->special_url = $special_url->url;
        }

        $categories = PropertyCategory::orderBy('position', 'desc')->get();
        $agents = TeamMember::orderBy('position', 'desc')->get();

        $images = array();
        $floorplans = array();

        if(old('imageCount')){

            for($i=0; $i<old('imageCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('imgId'.$i);
                $properties->position = old('imgPosition'.$i);
                $properties->delete = old('imgDelete'.$i);
                $properties->status = old('imgStatus'.$i);
                $properties->location = old('imgLocation'.$i);
                $properties->type = old('imgType'.$i);

                $images[$i] = $properties;
            }

            $property->images = collect($images)->sortBy('position');
        }

        if(old('floorplanCount')){

            for($i=0; $i<old('floorplanCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('flrId'.$i);
                $properties->position = old('flrPosition'.$i);
                $properties->delete = old('flrDelete'.$i);
                $properties->status = old('flrStatus'.$i);
                $properties->location = old('flrLocation'.$i);
                $properties->type = old('flrType'.$i);

                $floorplans[$i] = $properties;
            }

            $property->floorplans = collect($floorplans)->sortBy('position');
        }

        return view('admin/properties/edit', array(
            'property' => $property,
            'categories' => $categories,
            'agents' => $agents,
            'images' => $images,
            'floorplans' => $floorplans
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'slug' => 'required|unique_store:properties',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
            'property_type' => 'required',
            'street_number' => 'required',
            'street_name' => 'required',
            'suburb' => 'required',
            'ensuites' => 'nullable|numeric',
            'toilets' => 'nullable|numeric',
            'garage_spaces' => 'nullable|numeric',
            'carport_spaces' => 'nullable|numeric',
            'open_spaces' => 'nullable|numeric',
            'living_areas' => 'nullable|numeric',
            'house_size' => 'nullable|numeric',
            'land_size' => 'nullable|numeric',
            'headline' => 'required',
            'description' => 'required'
        );

        $category_slug = PropertyCategory::where('id','=',$request->category_id)->value('slug');

        if($category_slug=='sale'){
            $rules['price'] = 'required';
        }

        if($category_slug=='rent'){
            $rules['rental_per_week'] = 'required';
            $rules['date_available'] = 'required';
        }

        $messages = [
            'title.required' => 'Please select title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken',
            'property_type.required' => 'Please select property type',
            'price.required' => 'Please enter price',
            'rental_per_week.required' => 'Please enter rent per week',
            'date_available.required' => 'Please enter date available',
            'street_number.required' => 'Please enter street number',
            'street_name.required' => 'Please enter street name',
            'suburb.required' => 'Please enter suburb',
            'ensuites.numeric' => 'Please enter numeric value',
            'toilets.numeric' => 'Please enter numeric value',
            'garage_spaces.numeric' => 'Please enter numeric value',
            'carport_spaces.numeric' => 'Please enter numeric value',
            'open_spaces.numeric' => 'Please enter numeric value',
            'living_areas.numeric' => 'Please enter numeric value',
            'house_size.numeric' => 'Please enter numeric value',
            'land_size.numeric' => 'Please enter numeric value',
            'headline.required' => 'Please enter headline',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/properties/add')->withErrors($validator)->withInput();
        }

        if($request->hide_street_address=='Hide street address'){
            $hide_street_address = 'true';
        }else{
            $hide_street_address = 'false';
        }

        if($request->hide_street_view=='Hide street view'){
            $hide_street_view = 'true';
        }else{
            $hide_street_view = 'false';
        }

        $property = new Property();
        $property->category_id = $request->category_id;
        $property->title = $request->title;
        $property->slug = $request->slug;
        $property->meta_title = $request->meta_title;
        $property->meta_keywords = $request->meta_keywords;
        $property->meta_description = $request->meta_description;
        $property->property_type = $request->property_type;
        $property->new_or_established = $request->new_or_established;
        $property->lead_agent_id = $request->lead_agent_id;
        $property->dual_agent_id = $request->dual_agent_id;
        $property->authority = $request->authority;

        if($category_slug=='rent') {
            $property->rental_per_week = str_replace(',', '', $request->rental_per_week);
            $property->rental_per_month = str_replace(',', '', $request->rental_per_month);
            $property->security_bond = str_replace(',', '', $request->security_bond);
            $property->date_available = Carbon::createFromFormat('d/m/Y',$request->date_available)->format('Y-m-d');
            $property->landlord_name = $request->landlord_name;
            $property->landlord_email = $request->landlord_email;
            $property->landlord_phone_number = $request->landlord_phone_number;
        }
        if($category_slug=='sale') {
            $property->price = str_replace(',', '', $request->price);
        }

        $property->price_text = $request->price_text;
        $property->price_display = $request->price_display;
        $property->unit = $request->unit;
        $property->street_number = $request->street_number;
        $property->street_name = $request->street_name;
        $property->suburb = $request->suburb;
        $property->municipality = $request->municipality;
        $property->hide_street_address = $hide_street_address;
        $property->hide_street_view = $hide_street_view;
        $property->bedrooms = $request->bedrooms;
        $property->bathrooms = $request->bathrooms;
        $property->ensuites = $request->ensuites;
        $property->toilets = $request->toilets;
        $property->garage_spaces = $request->garage_spaces;
        $property->carport_spaces = $request->carport_spaces;
        $property->open_spaces = $request->open_spaces;
        $property->living_areas = $request->living_areas;
        $property->house_size = $request->house_size;
        $property->house_size_unit = $request->house_size_unit;
        $property->land_size = $request->land_size;
        $property->land_size_unit = $request->land_size_unit;
        $property->enery_efficiency_rating = $request->enery_efficiency_rating;
        $property->outdoor_features = $this->convertCheckboxesToJson($request->outdoor_features);
        $property->indoor_features = $this->convertCheckboxesToJson($request->indoor_features);
        $property->heating_cooling = $this->convertCheckboxesToJson($request->heating_cooling);
        $property->eco_friendly_features = $this->convertCheckboxesToJson($request->eco_friendly_features);
        $property->other_features = $request->other_features;
        $property->headline = $request->headline;
        $property->description = $request->description;
        $property->statement_of_information_pdf = $request->statement_of_information_pdf;
        $property->inspections_data = $request->inspections_data;
        $property->status = 'passive';
        $property->save();

        ///////////special URL//
        if ($request->special_url != "") {

            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $property->id;
            $new_special_url->module = 'properties';
            $new_special_url->type = 'item';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();
        }
        ////////////////////////

        $this->storeImages($request, $property->id);
        $this->storeFloorplans($request, $property->id);

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/properties/' . $property->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/properties')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }
    }

    public function update(Request $request)
    {

        $rules = array(
            'title' => 'required',
            'slug' => 'required|unique_update:properties,' . $request->id,
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:properties,item,' . $request->id,
            'property_type' => 'required',
            'street_number' => 'required',
            'street_name' => 'required',
            'suburb' => 'required',
            'ensuites' => 'nullable|numeric',
            'toilets' => 'nullable|numeric',
            'garage_spaces' => 'nullable|numeric',
            'carport_spaces' => 'nullable|numeric',
            'open_spaces' => 'nullable|numeric',
            'living_areas' => 'nullable|numeric',
            'house_size' => 'nullable|numeric',
            'land_size' => 'nullable|numeric',
            'headline' => 'required',
            'description' => 'required'
        );

        $category_slug = PropertyCategory::where('id','=',$request->category_id)->value('slug');

        if($category_slug=='sale'){
            $rules['price'] = 'required';
        }

        if($category_slug=='rent'){
            $rules['rental_per_week'] = 'required';
            $rules['date_available'] = 'required';
        }

        $messages = [
            'title.required' => 'Please select title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_update' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken',
            'property_type.required' => 'Please select property type',
            'price.required' => 'Please enter price',
            'rental_per_week.required' => 'Please enter rent per week',
            'date_available.required' => 'Please enter date available',
            'street_number.required' => 'Please enter street number',
            'street_name.required' => 'Please enter street name',
            'suburb.required' => 'Please enter suburb',
            'ensuites.numeric' => 'Please enter numeric value',
            'toilets.numeric' => 'Please enter numeric value',
            'garage_spaces.numeric' => 'Please enter numeric value',
            'carport_spaces.numeric' => 'Please enter numeric value',
            'open_spaces.numeric' => 'Please enter numeric value',
            'living_areas.numeric' => 'Please enter numeric value',
            'house_size.numeric' => 'Please enter numeric value',
            'land_size.numeric' => 'Please enter numeric value',
            'headline.required' => 'Please enter headline',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/properties/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        if($request->hide_street_address=='Hide street address'){
            $hide_street_address = 'true';
        }else{
            $hide_street_address = 'false';
        }

        if($request->hide_street_view=='Hide street view'){
            $hide_street_view = 'true';
        }else{
            $hide_street_view = 'false';
        }

        $property = Property::where('id','=',$request->id)->first();
        $property->category_id = $request->category_id;
        $property->title = $request->title;
        $property->slug = $request->slug;
        $property->meta_title = $request->meta_title;
        $property->meta_keywords = $request->meta_keywords;
        $property->meta_description = $request->meta_description;
        $property->property_type = $request->property_type;
        $property->new_or_established = $request->new_or_established;
        $property->lead_agent_id = $request->lead_agent_id;
        $property->dual_agent_id = $request->dual_agent_id;
        $property->authority = $request->authority;

        if($category_slug=='rent') {
            $property->rental_per_week = str_replace(',', '', $request->rental_per_week);
            $property->rental_per_month = str_replace(',', '', $request->rental_per_month);
            $property->security_bond = str_replace(',', '', $request->security_bond);
            $property->date_available = Carbon::createFromFormat('d/m/Y',$request->date_available)->format('Y-m-d');
            $property->landlord_name = $request->landlord_name;
            $property->landlord_email = $request->landlord_email;
            $property->landlord_phone_number = $request->landlord_phone_number;
        }
        if($category_slug=='sale') {
            $property->price = str_replace(',', '', $request->price);
        }

        $property->price_text = $request->price_text;
        $property->price_display = $request->price_display;
        $property->unit = $request->unit;
        $property->street_number = $request->street_number;
        $property->street_name = $request->street_name;
        $property->suburb = $request->suburb;
        $property->municipality = $request->municipality;
        $property->hide_street_address = $hide_street_address;
        $property->hide_street_view = $hide_street_view;
        $property->bedrooms = $request->bedrooms;
        $property->bathrooms = $request->bathrooms;
        $property->ensuites = $request->ensuites;
        $property->toilets = $request->toilets;
        $property->garage_spaces = $request->garage_spaces;
        $property->carport_spaces = $request->carport_spaces;
        $property->open_spaces = $request->open_spaces;
        $property->living_areas = $request->living_areas;
        $property->house_size = $request->house_size;
        $property->house_size_unit = $request->house_size_unit;
        $property->land_size = $request->land_size;
        $property->land_size_unit = $request->land_size_unit;
        $property->enery_efficiency_rating = $request->enery_efficiency_rating;
        $property->outdoor_features = $this->convertCheckboxesToJson($request->outdoor_features);
        $property->indoor_features = $this->convertCheckboxesToJson($request->indoor_features);
        $property->heating_cooling = $this->convertCheckboxesToJson($request->heating_cooling);
        $property->eco_friendly_features = $this->convertCheckboxesToJson($request->eco_friendly_features);
        $property->other_features = $request->other_features;
        $property->headline = $request->headline;
        $property->description = $request->description;
        $property->statement_of_information_pdf = $request->statement_of_information_pdf;
        $property->inspections_data = $request->inspections_data;
        $property->save();

        //////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $property->id)->where('module', '=', 'properties')->where('type', '=', 'item')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();
            } else {
                $special_url->delete();
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $property->id;
                $new_special_url->module = 'properties';
                $new_special_url->type = 'item';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();
            }
        }
        ////////////////////////

        $this->storeImages($request, $property->id);
        $this->storeFloorplans($request, $property->id);

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/properties/' . $property->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/properties')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }

    }

    public function storeImages(Request $request, $property_id)
    {

        $deleteCount = 0;

        for($i = 0; $i < $request->imageCount; $i++)  {
            if (isset($request["imgDelete" . $i]) && $request["imgDelete" . $i] == 1)  {

                if($request["imgType" . $i]!="new"){
                    // Delete Image
                    $property_image = PropertyImage::where('id','=',$request["imgId" . $i])->first();
                    $property_image->delete();
                    $deleteCount++;
                }

            } else  {
                // Save Image (New or Update)
                if($request["imgType" . $i]=="new"){
                    $image = new PropertyImage();
                } else  {
                    $image = PropertyImage::where('id','=',$request["imgId" . $i])->first();
                }

                $image->property_id = $property_id;
                $image->location = $request["imgLocation" . $i];
                $image->position = $request["imgPosition" . $i] - $deleteCount;
                if($request["imgStatus" . $i]=='on'){
                    $image->status = 'active';
                } else {
                    $image->status = 'passive';
                }

                $image->save();
            }
        }
    }

    public function storeFloorplans(Request $request, $property_id)
    {

        $deleteCount = 0;

        for($i = 0; $i < $request->floorplanCount; $i++)  {
            if (isset($request["flrDelete" . $i]) && $request["flrDelete" . $i] == 1)  {

                if($request["flrType" . $i]!="new"){
                    // Delete Image
                    $property_floorplan = PropertyFloorplan::where('id','=',$request["flrId" . $i])->first();
                    $property_floorplan->delete();
                    $deleteCount++;
                }

            } else  {
                // Save Image (New or Update)
                if($request["flrType" . $i]=="new"){
                    $floorplan = new PropertyFloorplan();
                } else  {
                    $floorplan = PropertyFloorplan::where('id','=',$request["flrId" . $i])->first();
                }

                $floorplan->property_id = $property_id;
                $floorplan->location = $request["flrLocation" . $i];
                $floorplan->position = $request["flrPosition" . $i] - $deleteCount;
                if($request["flrStatus" . $i]=='on'){
                    $floorplan->status = 'active';
                } else {
                    $floorplan->status = 'passive';
                }

                $floorplan->save();
            }
        }
    }

    public function delete($property_id)
    {
        $property = Property::where('id','=',$property_id)->first();
        $property->is_deleted = true;
        $property->save();

        SpecialUrl::where('item_id', '=', $property->id)->where('module', '=', 'properties')->where('type', '=', 'item')->delete();

        return \Redirect::back()->with('message', Array('text' => 'Property has been deleted.', 'status' => 'success'));
    }

    public function changePropertyStatus(Request $request, $property_id)
    {
        $property = Property::where('id', '=', $property_id)->first();
        if ($request->status == "true") {
            $property->status = 'active';
        } else if ($request->status == "false") {
            $property->status = 'passive';
        }
        $property->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $properties = Property::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/properties/sort', array(
            'properties' => $properties
        ));
    }

    public function categories(Request $request)
    {
        $categories = PropertyCategory::orderBy('position', 'desc')->get();
        return view('admin/properties/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/properties/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:property_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store'
        );

        $messages = [
            'name.required' => 'Please enter name.',
            'slug.unique_store' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/properties/add-category')->withErrors($validator)->withInput();
        }

        $category = new PropertyCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->description = $request->description;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();

        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'properties';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        (new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'properties', 'category', $category->slug);

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/properties/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/properties/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        }

    }

    public function editCategory($category_id)
    {
        $category = PropertyCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'properties')->where('type', '=', 'category')->first();
        if ($special_url) {
            $category->special_url = $special_url->url;
        }

        return view('admin/properties/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:property_categories,'.$request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:properties,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter name.',
            'slug.unique_update' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/properties/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = PropertyCategory::findOrFail($request->id);

        (new NavigationHelper())->navigationItems('update', 'category', 'properties', $category->slug, null, $request->name, $request->slug);

        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->description = $request->description;
        if($request->live=='on'){
            $category->status = 'active';

            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'properties', 'category', $category->slug);
        } else {
            $category->status = 'passive';
            (new NavigationHelper())->navigationItems('delete-item', 'category', 'properties', $category->slug);
        }
        $category->save();

        (new NavigationHelper())->navigationItems('change-href', 'category', 'properties', $category->slug, $category->url);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'properties')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'properties', $category->slug, $request->special_url);
            } else {
                $special_url->delete();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'properties', $category->slug, 'properties/'.$category->slug);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'properties';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'properties', $category->slug, $request->special_url);
            }
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/properties/' . $category->id . '/edit-type')->with('message', Array('text' => 'Category has been update', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/properties/categories')->with('message', Array('text' => 'Category has been update', 'status' => 'success'));
        }

    }

    public function deleteCategory($category_id)
    {
        $category = PropertyCategory::where('id','=',$category_id)->first();

        if(count($category->properties)){
            return \Redirect::to('dreamcms/properties/categories')->with('message', Array('text' => 'Category has properties. Please delete properties first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'properties')->where('type', '=', 'category')->delete();

        (new NavigationHelper())->navigationItems('delete-item', 'category', 'properties', $category->slug);

        return \Redirect::to('dreamcms/properties/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('properties-filter');
        return redirect()->to('dreamcms/properties');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->type && $request->type != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('properties-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('properties-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

    public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = PropertyCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';

            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'properties', 'category', $category->slug);

        } else if ($request->status == "false") {
            $category->status = 'passive';

            (new NavigationHelper())->navigationItems('delete-item', 'category', 'properties', $category->slug);
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = PropertyCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/properties/sort-category', array(
            'categories' => $categories
        ));
    }

    private function convertCheckboxesToJson($checkboxes){

        $json_text = "[";

        if(count($checkboxes)>0){
            $counter = 0;
            foreach ($checkboxes as $checkbox){
                $json_text .= '{"'.$counter.'": "'.$checkbox.'"},';
                $counter++;
            }
            $json_text = rtrim($json_text,", ");
        }

        $json_text .= "]";
        return $json_text;
    }

}