<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Page;
use App\PageCategory;
use App\SpecialUrl;

class PagesController extends Controller
{
    public function index($category_slug, $page_slug = "", $mode = ""){

    	$category = $this->getCategory($category_slug);
		//$side_nav = $this->getPages($category[0]->id);
		//$page = ($page_slug == "" ? $this->getPages($category[0]->id)->first() : $this->getPage($category[0]->id, $page_slug, $mode));

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $page = ($page_slug == "" ? $this->getFirstPage($side_nav) : $this->getPage($category[0]->id, $page_slug, $mode));

		return view('site/pages/pages', array(
            'page_type' => "Pages",
			'side_nav' => $side_nav,
			'category' => $category,			
			'page' => $page,
			'mode' => $mode,
        ));
    }
	
	public function getCategory($category_slug){
		$categories = PageCategory::where('slug', '=', $category_slug)->orderBy('position', 'asc')->get();		
		return($categories);
	}
	
	public function getPages($category_id){
		$pages = Page::where('status', '=', 'active')->where('category_id', '=', $category_id)->whereNull('parent_page_id')->orderBy('position', 'asc')->get();		
		
		foreach ($pages as $page):
		   $page['nav_sub'] = $this->getSubPages($page->id);	
		endforeach;
		
		return($pages);
	}
	
	public function getSubPages($page_id){
		$pages = Page::where('status', '=', 'active')->where('parent_page_id', '=', $page_id)->orderBy('position', 'asc')->get();		
		return($pages);
	}	
	
	public function getPage($category_id, $page_slug, $mode){
		if ($mode == "preview") {
		   $pages = Page::where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();	
		} else {
		   $pages = Page::where('status', '=', 'active')->where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();		
		}
		
		return($pages);
	}

	private function getFirstPage($side_nav){
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $side_nav, $match);
        $url_arr = array_reverse(explode("/",$page_url = $match[0][0]));
		
        $pages = Page::where('slug','=',$url_arr[0])->first();
		
		if ($pages == null)  {
		   $specialURL = SpecialUrl::where('url', '=', $url_arr[0])->where('module', '=', 'pages')->where('type', '=', 'item')->first();
		   $pages = Page::where('id','=',$specialURL->item_id)->first();
		}
		return($pages);
    }
}
