<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Project extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'projects';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(ProjectCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(ProjectCategory::class,'id','category_id');
    }

	public function images()
    {
        return $this->hasMany(ProjectImage::class, 'project_id')->orderBy('position', 'asc');
    }
	
    public function scopeFilter($query)
    {

        $filter = session()->get('projects-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);			
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','projects')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'projects/'.$this->category->slug.'/'.$this->attributes['slug'];
    }
}
