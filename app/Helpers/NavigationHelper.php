<?php
namespace App\Helpers;

use App\Module;
use App\Navigation;
use App\PageCategory;

class NavigationHelper {


    public function navigationItems($mode, $type, $module_slug, $slug, $href=null, $new_name=null, $new_slug=null, $children=null)
    {
        $navigation_raw = Navigation::where('id', '=', 1)->first();
        $navigation = json_decode($navigation_raw->navigation);

        $existence_control[0] = false;
        $existence_control[1] = '';

        foreach($navigation as $key => $element){

            //lvl 1
            //echo $element->text."<br>";
            if($element->type==$type && $element->module==$module_slug && $element->slug==$slug){
                $existence_control[0] = true;
                $existence_control[1] = $key;

                if($mode=='change-href'){
                    $element->href = $href;
                }

                if($mode=='delete-item'){
                    array_splice($navigation, $key, 1);
                }

                if($mode=='update'){
                    $element->text = $new_name;
                    $element->slug = $new_slug;
                }

                if($mode=='add-pages'){
                    $element->children = json_decode(json_encode($children));
                }
            }

            if($mode=='side-nav') {

                if($element->href==$href){

                    if(isset($element->children)){
                        return $element->children;
                    }
                    return null;
                }
            }

            if(isset($element->children)){

                foreach ($element->children as $key_lvl1 => $children_lvl1){

                    //lvl2
                    //echo '-'.$children_lvl1->text."<br>";
                    if($children_lvl1->type==$type && $children_lvl1->module==$module_slug && $children_lvl1->slug==$slug){
                        $existence_control[0] = true;
                        $existence_control[1] = $children_lvl1;

                        if($mode=='change-href'){
                            $children_lvl1->href = $href;
                        }

                        if($mode=='delete-item'){
                            array_splice($element->children, $key_lvl1, 1);

                            if(count($element->children)==0){
                                unset($element->children);
                            }
                        }

                        if($mode=='update'){
                            $children_lvl1->text = $new_name;
                            $children_lvl1->slug = $new_slug;
                        }

                        if($mode=='add-pages'){
                            $children_lvl1->children = json_decode(json_encode($children));
                        }

                    }

                    if($mode=='side-nav') {
                        if($children_lvl1->href==$href){
                            return $element->children;
                        }
                    }

                    if(isset($children_lvl1->children)){

                        foreach($children_lvl1->children as $key_lvl2 => $children_lvl2){

                            //lvl3
                            //echo '--'.$children_lvl2->text."<br>";
                            if($children_lvl2->type==$type && $children_lvl2->module==$module_slug && $children_lvl2->slug==$slug){
                                $existence_control[0] = true;
                                $existence_control[1] = $children_lvl2;

                                if($mode=='change-href'){
                                    $children_lvl2->href = $href;
                                }

                                if($mode=='delete-item'){
                                    array_splice($children_lvl1->children, $key_lvl2, 1);

                                    if(count($children_lvl1->children)==0){
                                        unset($children_lvl1->children);
                                    }
                                }

                                if($mode=='update'){
                                    $children_lvl2->text = $new_name;
                                    $children_lvl2->slug = $new_slug;
                                }

                                if($mode=='add-pages'){
                                    $children_lvl2->children = json_decode(json_encode($children));
                                }
                            }

                            if($mode=='side-nav') {
                                if($children_lvl2->href==$href){
                                    return $element->children;
                                }
                            }

                            if(isset($children_lvl2->children)){

                                foreach($children_lvl2->children as $key_lvl3 => $children_lvl3){

                                    //lvl4
                                    //echo '---'.$children_lvl3->text."<br>";
                                    if($children_lvl3->type==$type && $children_lvl3->module==$module_slug && $children_lvl3->slug==$slug){
                                        $existence_control[0] = true;
                                        $existence_control[1] = $children_lvl3;

                                        if($mode=='change-href'){
                                            $children_lvl3->href = $href;
                                        }

                                        if($mode=='delete-item'){
                                            array_splice($children_lvl2->children, $key_lvl3, 1);

                                            if(count($children_lvl2->children)==0){
                                                unset($children_lvl2->children);
                                            }
                                        }

                                        if($mode=='update'){
                                            $children_lvl3->text = $new_name;
                                            $children_lvl3->slug = $new_slug;
                                        }

                                        if($mode=='add-pages'){
                                            $children_lvl3->children = json_decode(json_encode($children));
                                        }
                                    }

                                    if($mode=='side-nav') {
                                        if($children_lvl3->href==$href){
                                            return $element->children;
                                        }
                                    }

                                    if(isset($children_lvl3->children)){

                                        foreach($children_lvl3->children as $key_lvl4 => $children_lvl4){

                                            //lvl5
                                            //echo '-----'.$children_lvl4->text."<br>";
                                            if($children_lvl4->type==$type && $children_lvl4->module==$module_slug && $children_lvl4->slug==$slug){
                                                $existence_control[0] = true;
                                                $existence_control[1] = $children_lvl4;

                                                if($mode=='change-href'){
                                                    $children_lvl4->href = $href;
                                                }

                                                if($mode=='delete-item'){
                                                    array_splice($children_lvl3->children, $key_lvl4, 1);

                                                    if(count($children_lvl3->children)==0){
                                                        unset($children_lvl3->children);
                                                    }
                                                }

                                                if($mode=='update'){
                                                    $children_lvl4->text = $new_name;
                                                    $children_lvl4->slug = $new_slug;
                                                }
                                            }

                                            if($mode=='side-nav') {
                                                if($children_lvl4->href==$href){
                                                    return $element->children;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if($mode=='change-href' || $mode=='delete-item' || $mode=='update' || $mode=='add-pages'){

            $navigation_raw->navigation = json_encode($navigation);
            $navigation_raw->save();
        }

        return $existence_control;
    }

    public function navigationAddItem($text, $href, $id, $module, $type, $slug)
    {

        $module_db = Module::where('slug','=', $module)->first();
        $existence = $this->navigationItems('existence', $type, $module_db->slug, $slug);

        if($existence[0]!=true){

            $navigation_raw = Navigation::where('id', '=', 1)->first();
            $navigation = json_decode($navigation_raw->navigation);

            $navigation[] = array(
                'text' => $text,
                'icon' => '',
                'href' => $href,
                'target' => 'self',
                'title' => '',
                'id' => $id,
                'module' => $module,
                'type' => $type,
                'slug' => $slug,
            );

            $navigation_raw->navigation = json_encode($navigation);
            $navigation_raw->save();
        }
    }

    public function navigationAddPagesCategory($text, $href, $id, $slug)
    {
        $module = 'pages';
        $type = 'category';

        $existence = $this->navigationItems('existence', $type, 'pages', $slug);

        if($existence[0]!=true){

            $navigation_raw = Navigation::where('id', '=', 1)->first();
            $navigation = json_decode($navigation_raw->navigation);

            $navigation[] = array(
                'text' => $text,
                'icon' => '',
                'href' => $href,
                'target' => 'self',
                'title' => '',
                'id' => $id,
                'module' => $module,
                'type' => $type,
                'slug' => $slug,
            );

            $category = PageCategory::where('slug','=',$slug)->first();

            if(count($category->pages)>0){

                foreach($category->pages as $page){

                    $children[] = array(
                        'text' => $page->title,
                        'icon' => '',
                        'href' => $page->url,
                        'target' => 'self',
                        'title' => '',
                        'id' => 'page-'.$page->id,
                        'module' => $module,
                        'type' => 'page',
                        'slug' => $page->slug,
                    );
                }

                end($navigation);
                $key = key($navigation);

                $navigation[$key]['children'] = $children;
            }

            $navigation_raw->navigation = json_encode($navigation);
            $navigation_raw->save();
        }
    }

    public function navigationUpdateCategoryPages($category_id)
    {

        $category = PageCategory::where('id','=',$category_id)->first();
        $existence = $this->navigationItems('existence', 'category', 'pages', $category->slug);

        if($existence[0]==true){

            if($category->has('pages')){

                foreach($category->pages as $page){

                    $children[] = array(
                        'text' => $page->title,
                        'icon' => '',
                        'href' => $page->url,
                        'target' => 'self',
                        'title' => '',
                        'id' => 'page-'.$page->id,
                        'module' => 'pages',
                        'type' => 'page',
                        'slug' => $page->slug,
                    );
                }
            }

            $this->navigationItems('add-pages', 'category', 'pages', $category->slug, null, null, null, $children);
        }
    }
}
?>