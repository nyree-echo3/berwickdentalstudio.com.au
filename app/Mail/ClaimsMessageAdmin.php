<?php

namespace App\Mail;

use App\ClaimsMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class ClaimsMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $claims_message;

    public function __construct(ClaimsMessages $claims_message)
    {
        $this->claims_message = $claims_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | Claims Form')
			        ->from($contactEmail)
			        ->view('site/emails/claims-message-admin');
    }
}
