$(document).ready(function() {

	var sliderState = 0;

    $('.contact-slider-button').click(function() {

		if(sliderState==0) {
	         $('#contact-slider').animate({'right': '0px'}, 500);
	         sliderState = 1;
	    }else if(sliderState==1) {
	         $('#contact-slider').animate({'right': '-648px'}, 500);
	         sliderState = 0;
	    }

    });

    $( "#sideContactForm" ).submit(function( event ) {
        ga('send', 'event', {
            eventCategory: 'Side Contact Form Submit',
            eventAction: 'click',
            eventLabel: 'Side Contact Form'
        });
    });

//    $('#contact-toogle-link').click(function() {
//		if(sliderState==0) {
//	         $('#contact-slider').animate({'right': '0px'}, 500);
//	         sliderState = 1;
//	    }
//
//    });

});

function btnContactToogle_onclick()  {	
   if ($('#contact-slider').css('display') == 'none' ) {	
	  document.location.href = "https://www.berwickdentalstudio.com.au/contact";
   } else  {
      $('#contact-slider').animate({'right': '0px'}, 500);	         	        
   }
}